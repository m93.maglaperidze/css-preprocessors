const { src, dest, watch, series } = require("gulp");
const sass = require("gulp-sass")(require("sass"));

// Compile Sass
function buildStyles() {
  return src("style.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(dest("css"));
}

// Watch for changes in Sass files
function watchTask() {
  watch(["style.scss"], buildStyles);
}

exports.default = series(buildStyles, watchTask);
